﻿namespace Biblioteka
{
    partial class UnosKnjiga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImeKnjige = new System.Windows.Forms.TextBox();
            this.AutorKnjige = new System.Windows.Forms.TextBox();
            this.IzdavacKnjige = new System.Windows.Forms.TextBox();
            this.BrojStranicaKnjige = new System.Windows.Forms.TextBox();
            this.PotvrdiDugme = new System.Windows.Forms.Button();
            this.OdustaniDugme = new System.Windows.Forms.Button();
            this.Zanr = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ImeKnjige
            // 
            this.ImeKnjige.Location = new System.Drawing.Point(65, 30);
            this.ImeKnjige.Name = "ImeKnjige";
            this.ImeKnjige.Size = new System.Drawing.Size(100, 20);
            this.ImeKnjige.TabIndex = 0;
            this.ImeKnjige.TextChanged += new System.EventHandler(this.ImeKnjige_TextChanged);
            // 
            // AutorKnjige
            // 
            this.AutorKnjige.Location = new System.Drawing.Point(65, 80);
            this.AutorKnjige.Name = "AutorKnjige";
            this.AutorKnjige.Size = new System.Drawing.Size(100, 20);
            this.AutorKnjige.TabIndex = 1;
            this.AutorKnjige.TextChanged += new System.EventHandler(this.AutorKnjige_TextChanged);
            // 
            // IzdavacKnjige
            // 
            this.IzdavacKnjige.Location = new System.Drawing.Point(65, 168);
            this.IzdavacKnjige.Name = "IzdavacKnjige";
            this.IzdavacKnjige.Size = new System.Drawing.Size(100, 20);
            this.IzdavacKnjige.TabIndex = 3;
            this.IzdavacKnjige.TextChanged += new System.EventHandler(this.IzdavacKnjige_TextChanged);
            // 
            // BrojStranicaKnjige
            // 
            this.BrojStranicaKnjige.Location = new System.Drawing.Point(65, 220);
            this.BrojStranicaKnjige.Name = "BrojStranicaKnjige";
            this.BrojStranicaKnjige.Size = new System.Drawing.Size(100, 20);
            this.BrojStranicaKnjige.TabIndex = 4;
            this.BrojStranicaKnjige.TextChanged += new System.EventHandler(this.BrojStranicaKnjige_TextChanged);
            // 
            // PotvrdiDugme
            // 
            this.PotvrdiDugme.Location = new System.Drawing.Point(22, 286);
            this.PotvrdiDugme.Name = "PotvrdiDugme";
            this.PotvrdiDugme.Size = new System.Drawing.Size(75, 23);
            this.PotvrdiDugme.TabIndex = 5;
            this.PotvrdiDugme.Text = "Potvrdi";
            this.PotvrdiDugme.UseVisualStyleBackColor = true;
            this.PotvrdiDugme.Click += new System.EventHandler(this.PotvrdiDugme_Click);
            // 
            // OdustaniDugme
            // 
            this.OdustaniDugme.Location = new System.Drawing.Point(173, 286);
            this.OdustaniDugme.Name = "OdustaniDugme";
            this.OdustaniDugme.Size = new System.Drawing.Size(75, 23);
            this.OdustaniDugme.TabIndex = 6;
            this.OdustaniDugme.Text = "Odustani";
            this.OdustaniDugme.UseVisualStyleBackColor = true;
            this.OdustaniDugme.Click += new System.EventHandler(this.OdustaniDugme_Click);
            // 
            // Zanr
            // 
            this.Zanr.FormattingEnabled = true;
            this.Zanr.Items.AddRange(new object[] {
            "Science - fiction",
            "Thriller",
            "Horror",
            "Romance",
            "Western",
            "History",
            "Mystery",
            "Musical",
            "Biography",
            "Adventure",
            "Action",
            "Documentary",
            "Drama",
            "Sport",
            "Family"});
            this.Zanr.Location = new System.Drawing.Point(65, 123);
            this.Zanr.Name = "Zanr";
            this.Zanr.Size = new System.Drawing.Size(100, 21);
            this.Zanr.TabIndex = 7;
            this.Zanr.SelectedIndexChanged += new System.EventHandler(this.Zanr_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Ime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(99, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Autor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Izdavac";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(99, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Zanr";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(82, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Broj stranica";
            // 
            // UnosKnjiga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 375);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Zanr);
            this.Controls.Add(this.OdustaniDugme);
            this.Controls.Add(this.PotvrdiDugme);
            this.Controls.Add(this.BrojStranicaKnjige);
            this.Controls.Add(this.IzdavacKnjige);
            this.Controls.Add(this.AutorKnjige);
            this.Controls.Add(this.ImeKnjige);
            this.Name = "UnosKnjiga";
            this.Text = "Unos knjiga";
            this.Load += new System.EventHandler(this.ImeKnjige_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ImeKnjige;
        private System.Windows.Forms.TextBox AutorKnjige;
        private System.Windows.Forms.TextBox IzdavacKnjige;
        private System.Windows.Forms.TextBox BrojStranicaKnjige;
        private System.Windows.Forms.Button PotvrdiDugme;
        private System.Windows.Forms.Button OdustaniDugme;
        private System.Windows.Forms.ComboBox Zanr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
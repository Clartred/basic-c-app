﻿using Biblioteka.Model;
using Biblioteka.Servis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class UnosKnjiga : Form
    {
        public UnosKnjiga()
        {
            InitializeComponent();
        }


        private void ImeKnjige_Load(object sender, EventArgs e)
        {

        }

        private void PotvrdiDugme_Click(object sender, EventArgs e)
        {
            if (ImeKnjige.Text != "" && AutorKnjige.Text != "" && Zanr.Text != "" && IzdavacKnjige.Text != "" && BrojStranicaKnjige.Text != "")
            {
                //MessageBox.Show(ImeKnjige.Text + " " + AutorKnjige.Text + " " + IzdavacKnjige.Text + " " + BrojStranicaKnjige.Text);
                int brojStranica = 0;
                try
                {
                    //izjava
                    brojStranica = Int32.Parse(BrojStranicaKnjige.Text);
                    Knjiga knjiga = new Knjiga(ImeKnjige.Text, AutorKnjige.Text, Zanr.Text, IzdavacKnjige.Text, brojStranica);
                    KnjigaServis knjigaServis = new KnjigaServis();
                    knjigaServis.UnesiKnjigu(knjiga);

                   //Kraca verzija --->
                   //new KnjigaServis().UnesiKnjigu(new Knjiga(ImeKnjige.Text, AutorKnjige.Text, Zanr.Text, IzdavacKnjige.Text, brojStranica));
                    MessageBox.Show("Uspesno ste uneli novu knjigu.");
                    this.Close();

                }
                catch (FormatException)
                {
                    MessageBox.Show("Uneli ste slovo u broju stranica, molimo unesite broj!");
                }
            }
            else if((ImeKnjige.Text == "" || AutorKnjige.Text == "" || Zanr.Text == "" || IzdavacKnjige.Text == "" || BrojStranicaKnjige.Text == ""))
            {
                MessageBox.Show("Morate popuniti sva polja!");
            }


        }

        private void ImeKnjige_TextChanged(object sender, EventArgs e)
        {

        }


        private void AutorKnjige_TextChanged(object sender, EventArgs e)
        {

        }

        private void IzdavacKnjige_TextChanged(object sender, EventArgs e)
        {

        }

        private void BrojStranicaKnjige_TextChanged(object sender, EventArgs e)
        {

        }

        private void OdustaniDugme_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Zanr_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

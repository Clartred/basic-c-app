﻿namespace Biblioteka
{
    partial class IzmeniClana
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ime = new System.Windows.Forms.TextBox();
            this.Prezime = new System.Windows.Forms.TextBox();
            this.PotvrdiDugme = new System.Windows.Forms.Button();
            this.OdustaniDugme = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Adresa = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Ime
            // 
            this.Ime.Location = new System.Drawing.Point(54, 36);
            this.Ime.Name = "Ime";
            this.Ime.Size = new System.Drawing.Size(100, 20);
            this.Ime.TabIndex = 0;
            // 
            // Prezime
            // 
            this.Prezime.Location = new System.Drawing.Point(54, 98);
            this.Prezime.Name = "Prezime";
            this.Prezime.Size = new System.Drawing.Size(100, 20);
            this.Prezime.TabIndex = 1;
            // 
            // PotvrdiDugme
            // 
            this.PotvrdiDugme.Location = new System.Drawing.Point(12, 208);
            this.PotvrdiDugme.Name = "PotvrdiDugme";
            this.PotvrdiDugme.Size = new System.Drawing.Size(75, 23);
            this.PotvrdiDugme.TabIndex = 2;
            this.PotvrdiDugme.Text = "Potvrdi";
            this.PotvrdiDugme.UseVisualStyleBackColor = true;
            this.PotvrdiDugme.Click += new System.EventHandler(this.PotvrdiDugme_Click);
            // 
            // OdustaniDugme
            // 
            this.OdustaniDugme.Location = new System.Drawing.Point(144, 208);
            this.OdustaniDugme.Name = "OdustaniDugme";
            this.OdustaniDugme.Size = new System.Drawing.Size(75, 23);
            this.OdustaniDugme.TabIndex = 3;
            this.OdustaniDugme.Text = "Odustani";
            this.OdustaniDugme.UseVisualStyleBackColor = true;
            this.OdustaniDugme.Click += new System.EventHandler(this.OdustaniDugme_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Adresa";
            // 
            // Adresa
            // 
            this.Adresa.Location = new System.Drawing.Point(54, 152);
            this.Adresa.Name = "Adresa";
            this.Adresa.Size = new System.Drawing.Size(100, 20);
            this.Adresa.TabIndex = 7;
            // 
            // IzmeniClana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 286);
            this.Controls.Add(this.Adresa);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OdustaniDugme);
            this.Controls.Add(this.PotvrdiDugme);
            this.Controls.Add(this.Prezime);
            this.Controls.Add(this.Ime);
            this.Name = "IzmeniClana";
            this.Text = "Izmeni clana";
            this.Load += new System.EventHandler(this.IzmeniClana_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ime;
        private System.Windows.Forms.TextBox Prezime;
        private System.Windows.Forms.Button PotvrdiDugme;
        private System.Windows.Forms.Button OdustaniDugme;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Adresa;
    }
}
﻿namespace Biblioteka
{
    partial class Biblioteka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UnesiDugme = new System.Windows.Forms.Button();
            this.ObrisiDugme = new System.Windows.Forms.Button();
            this.IzmeniDugme = new System.Windows.Forms.Button();
            this.IzdavanjeDugme = new System.Windows.Forms.Button();
            this.VracanjeKnjige = new System.Windows.Forms.Button();
            this.VidiClanove = new System.Windows.Forms.Button();
            this.TabelaKnjiga = new System.Windows.Forms.DataGridView();
            this.IzadjiDugme = new System.Windows.Forms.Button();
            this.Stampanje = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaKnjiga)).BeginInit();
            this.SuspendLayout();
            // 
            // UnesiDugme
            // 
            this.UnesiDugme.Location = new System.Drawing.Point(12, 12);
            this.UnesiDugme.Name = "UnesiDugme";
            this.UnesiDugme.Size = new System.Drawing.Size(98, 39);
            this.UnesiDugme.TabIndex = 0;
            this.UnesiDugme.Text = "Unesi";
            this.UnesiDugme.UseVisualStyleBackColor = true;
            this.UnesiDugme.Click += new System.EventHandler(this.UnesiDugme_Click);
            // 
            // ObrisiDugme
            // 
            this.ObrisiDugme.Location = new System.Drawing.Point(12, 57);
            this.ObrisiDugme.Name = "ObrisiDugme";
            this.ObrisiDugme.Size = new System.Drawing.Size(98, 45);
            this.ObrisiDugme.TabIndex = 1;
            this.ObrisiDugme.Text = "Obrisi";
            this.ObrisiDugme.UseVisualStyleBackColor = true;
            this.ObrisiDugme.Click += new System.EventHandler(this.ObrisiDugme_Click);
            // 
            // IzmeniDugme
            // 
            this.IzmeniDugme.Location = new System.Drawing.Point(12, 108);
            this.IzmeniDugme.Name = "IzmeniDugme";
            this.IzmeniDugme.Size = new System.Drawing.Size(98, 39);
            this.IzmeniDugme.TabIndex = 2;
            this.IzmeniDugme.Text = "Izmeni";
            this.IzmeniDugme.UseVisualStyleBackColor = true;
            this.IzmeniDugme.Click += new System.EventHandler(this.IzmeniDugme_Click);
            // 
            // IzdavanjeDugme
            // 
            this.IzdavanjeDugme.Location = new System.Drawing.Point(12, 217);
            this.IzdavanjeDugme.Name = "IzdavanjeDugme";
            this.IzdavanjeDugme.Size = new System.Drawing.Size(98, 40);
            this.IzdavanjeDugme.TabIndex = 3;
            this.IzdavanjeDugme.Text = "Izdavanje";
            this.IzdavanjeDugme.UseVisualStyleBackColor = true;
            this.IzdavanjeDugme.Click += new System.EventHandler(this.IzdavanjeDugme_Click);
            // 
            // VracanjeKnjige
            // 
            this.VracanjeKnjige.Location = new System.Drawing.Point(12, 263);
            this.VracanjeKnjige.Name = "VracanjeKnjige";
            this.VracanjeKnjige.Size = new System.Drawing.Size(98, 36);
            this.VracanjeKnjige.TabIndex = 4;
            this.VracanjeKnjige.Text = "Vracanje";
            this.VracanjeKnjige.UseVisualStyleBackColor = true;
            this.VracanjeKnjige.Click += new System.EventHandler(this.VracanjeKnjige_Click);
            // 
            // VidiClanove
            // 
            this.VidiClanove.Location = new System.Drawing.Point(12, 153);
            this.VidiClanove.Name = "VidiClanove";
            this.VidiClanove.Size = new System.Drawing.Size(98, 40);
            this.VidiClanove.TabIndex = 5;
            this.VidiClanove.Text = "Vidi Clanove";
            this.VidiClanove.UseVisualStyleBackColor = true;
            this.VidiClanove.Click += new System.EventHandler(this.VidiClanove_Click);
            // 
            // TabelaKnjiga
            // 
            this.TabelaKnjiga.Location = new System.Drawing.Point(116, 12);
            this.TabelaKnjiga.Name = "TabelaKnjiga";
            this.TabelaKnjiga.Size = new System.Drawing.Size(833, 434);
            this.TabelaKnjiga.TabIndex = 0;
            this.TabelaKnjiga.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelaKnjiga_CellContentClick);
            // 
            // IzadjiDugme
            // 
            this.IzadjiDugme.Location = new System.Drawing.Point(12, 364);
            this.IzadjiDugme.Name = "IzadjiDugme";
            this.IzadjiDugme.Size = new System.Drawing.Size(98, 31);
            this.IzadjiDugme.TabIndex = 6;
            this.IzadjiDugme.Text = "Izadji";
            this.IzadjiDugme.UseVisualStyleBackColor = true;
            this.IzadjiDugme.Click += new System.EventHandler(this.IzadjiDugme_Click);
            // 
            // Stampanje
            // 
            this.Stampanje.Location = new System.Drawing.Point(12, 305);
            this.Stampanje.Name = "Stampanje";
            this.Stampanje.Size = new System.Drawing.Size(98, 35);
            this.Stampanje.TabIndex = 7;
            this.Stampanje.Text = "Stampanje";
            this.Stampanje.UseVisualStyleBackColor = true;
            this.Stampanje.Click += new System.EventHandler(this.Stampanje_Click);
            // 
            // Biblioteka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 449);
            this.Controls.Add(this.Stampanje);
            this.Controls.Add(this.IzadjiDugme);
            this.Controls.Add(this.TabelaKnjiga);
            this.Controls.Add(this.VidiClanove);
            this.Controls.Add(this.VracanjeKnjige);
            this.Controls.Add(this.IzdavanjeDugme);
            this.Controls.Add(this.IzmeniDugme);
            this.Controls.Add(this.ObrisiDugme);
            this.Controls.Add(this.UnesiDugme);
            this.Name = "Biblioteka";
            this.Text = "Knjige";
            this.Load += new System.EventHandler(this.Biblioteka_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaKnjiga)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button UnesiDugme;
        private System.Windows.Forms.Button ObrisiDugme;
        private System.Windows.Forms.Button IzmeniDugme;
        private System.Windows.Forms.Button IzdavanjeDugme;
        private System.Windows.Forms.Button VracanjeKnjige;
        private System.Windows.Forms.Button VidiClanove;
        private System.Windows.Forms.DataGridView TabelaKnjiga;
        private System.Windows.Forms.Button IzadjiDugme;
        private System.Windows.Forms.Button Stampanje;
    }
}


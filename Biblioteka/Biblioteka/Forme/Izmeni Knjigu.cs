﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteka.Model;
using Biblioteka.Servis;

namespace Biblioteka
{
    public partial class IzmeniKnjigu : Form
    {

        public Knjiga knjiga;

        public IzmeniKnjigu()
        {
            InitializeComponent();
        }

        private void IzmeniKnjige_Load(object sender, EventArgs e)
        {
            this.Naziv.Text = this.knjiga.Naziv;
            this.Autor.Text = this.knjiga.Autor;
            this.Izdavac.Text = this.knjiga.Izdavac;
            this.Zanr.Text = this.knjiga.Zanr;
            this.BrojStranica.Text = this.knjiga.BrojStranica.ToString();

        }

        private void Potvrdi_Click(object sender, EventArgs e)
        {
            KnjigaServis ks = new KnjigaServis();
            Knjiga k = new Knjiga(this.knjiga.Id, this.Naziv.Text, this.Autor.Text, this.Zanr.Text, this.Izdavac.Text,  Int32.Parse(this.BrojStranica.Text));
            ks.IzmeniKnjigu(k);
            this.Close();
        }

        private void Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Zanr_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

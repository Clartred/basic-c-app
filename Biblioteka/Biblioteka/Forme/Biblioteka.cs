﻿using Biblioteka.Model;
using Biblioteka.Servis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class Biblioteka : Form
    {
        int IdZaBrisanje = 0;
        private Knjiga knjigaZaIzmenu;

        public Biblioteka()
        {
            InitializeComponent();
        }

        private void Biblioteka_Load(object sender, EventArgs e)
        {
           // PopuniTabelu();
        }

        private void PopuniTabelu()
        {
            //tabelaKnjiga je promenljiva 
            KnjigaServis knjigaS = new KnjigaServis();
            TabelaKnjiga.DataSource = knjigaS.DajSveKnjige();
        }

        private Knjiga KonvertujKnjigu(DataRowView d)
        {
            return null;
        }

        private void ObrisiDugme_Click(object sender, EventArgs e)
        {
            //Ako IdZaBrisanje nije prazan, to znaci da smo oznacili zeljenu knjigu
            //koju zelimo da obrisemo

            if (IdZaBrisanje != 0)
            {   
                /*
                Duza verzija----->
                KnjigaServis knjigaS = new KnjigaServis();
                knjigaS.IzbrisiKnjigu(IdZaBrisanje);
                */
                new KnjigaServis().IzbrisiKnjigu(IdZaBrisanje);
            }
            IdZaBrisanje = 0;
            PopuniTabelu();
        }

        private void UnesiDugme_Click(object sender, EventArgs e)
        {
            var frm = new UnosKnjiga();

            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.ShowDialog();
            PopuniTabelu();
        }

        private void IzmeniDugme_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new IzmeniKnjigu();
            frm.knjiga = knjigaZaIzmenu;
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.ShowDialog();
            PopuniTabelu();
        }

        private void IzdavanjeDugme_Click(object sender, EventArgs e)
        {
            KnjigaServis ks = new KnjigaServis();
            ks.IzdavanjeKnjige(IdZaBrisanje);
            PopuniTabelu();
        }

        private void VracanjeKnjige_Click(object sender, EventArgs e)
        {
            KnjigaServis ks = new KnjigaServis();
            ks.VracanjeKnjige(IdZaBrisanje);
            PopuniTabelu();
        }


        private void VidiClanove_Click(object sender, EventArgs e)
        {
            Clanovi frm = new Clanovi();
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.ShowDialog(); 
        }

        private void TabelaKnjiga_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Knjiga k = KonvertujCelijuUKnjigu(e.RowIndex);
            this.knjigaZaIzmenu = k;
            IdZaBrisanje = k.Id;
            MessageBox.Show(k.Id +" "+ k.Autor+" "+k.Naziv+" "+k.Izdavac+" "+k.Zanr+" "+k.BrojStranica);
        }

        private Knjiga KonvertujCelijuUKnjigu(int i)
        {
            int id = Convert.ToInt32(TabelaKnjiga.Rows[i].Cells["id"].Value.ToString());
            string autor = TabelaKnjiga.Rows[i].Cells["autor"].Value.ToString();
            string naziv = TabelaKnjiga.Rows[i].Cells["naziv"].Value.ToString();
            string izdavac = TabelaKnjiga.Rows[i].Cells["izdavac"].Value.ToString();
            string zanr = TabelaKnjiga.Rows[i].Cells["zanr"].Value.ToString();
            int broj_stranica = Convert.ToInt32(TabelaKnjiga.Rows[i].Cells["broj_stranica"].Value.ToString());

            Knjiga k = new Knjiga(id, autor, naziv,  zanr, izdavac, broj_stranica);

            return k;

            /* Ovo je uprosceno---->
                        return new Knjiga(
                  Convert.ToInt32(TabelaKnjiga.Rows[i].Cells["id"].Value.ToString()),
                  (TabelaKnjiga.Rows[i].Cells["autor"].Value.ToString()),
                  (TabelaKnjiga.Rows[i].Cells["naziv"].Value.ToString()),
                  (TabelaKnjiga.Rows[i].Cells["izdavac"].Value.ToString()),
                  (TabelaKnjiga.Rows[i].Cells["zanr"].Value.ToString()),
                  Convert.ToInt32(TabelaKnjiga.Rows[i].Cells["broj_stranica"].Value.ToString())
                  ); */
        }


        private void IzadjiDugme_Click(object sender, EventArgs e)
        {
            //Kada korisnik klikne na dugme Izadji, automatski se izlazi iz aplikacije
            Application.Exit();
        }

        private void Stampanje_Click(object sender, EventArgs e)
        {
            Stampanje s = new Stampanje();
            List<Knjiga> l = new List<Knjiga>();
            //int id, string naziv, string autor, string zanr, string izdavac, int brojStranica)
            l.Add(new Knjiga(1, "naziv1", "autor1", "zanr1", "izdavac1", 1));
            l.Add(new Knjiga(2, "naziv2", "autor2", "zanr2", "izdavac2", 2));
            l.Add(new Knjiga(3, "naziv3", "autor3", "zanr3", "izdavac3", 3));
            l.Add(new Knjiga(4, "naziv4", "autor4", "zanr4", "izdavac4", 4));
            l.Add(new Knjiga(5, "naziv5", "autor5", "zanr5", "izdavac5", 5));
            l.Add(new Knjiga(6, "naziv6", "autor6", "zanr6", "izdavac6", 6));
            l.Add(new Knjiga(7, "naziv7", "autor7", "zanr7", "izdavac8", 7));
            l.Add(new Knjiga(8, "naziv8", "autor8", "zanr8", "izdavac9", 8));
            l.Add(new Knjiga(9, "naziv9", "autor9", "zanr9", "izdavac10", 9));
            l.Add(new Knjiga(10, "naziv10", "autor10", "zanr10", "izdavac1", 10));
            TabelaKnjiga.DataSource = l;
            s.Stampaj(TabelaKnjiga);
        }
    }
}

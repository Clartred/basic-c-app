﻿namespace Biblioteka
{
    partial class UnosClanova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImeClana = new System.Windows.Forms.TextBox();
            this.PrezimeClana = new System.Windows.Forms.TextBox();
            this.Jmbg = new System.Windows.Forms.TextBox();
            this.PotvrdiDugme = new System.Windows.Forms.Button();
            this.OdustaniDugme = new System.Windows.Forms.Button();
            this.Adresa = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ImeClana
            // 
            this.ImeClana.Location = new System.Drawing.Point(74, 39);
            this.ImeClana.Name = "ImeClana";
            this.ImeClana.Size = new System.Drawing.Size(100, 20);
            this.ImeClana.TabIndex = 0;
            this.ImeClana.TextChanged += new System.EventHandler(this.ImeClana_TextChanged);
            // 
            // PrezimeClana
            // 
            this.PrezimeClana.Location = new System.Drawing.Point(74, 87);
            this.PrezimeClana.Name = "PrezimeClana";
            this.PrezimeClana.Size = new System.Drawing.Size(100, 20);
            this.PrezimeClana.TabIndex = 1;
            this.PrezimeClana.TextChanged += new System.EventHandler(this.PrezimeClana_TextChanged);
            // 
            // Jmbg
            // 
            this.Jmbg.Location = new System.Drawing.Point(74, 135);
            this.Jmbg.Name = "Jmbg";
            this.Jmbg.Size = new System.Drawing.Size(100, 20);
            this.Jmbg.TabIndex = 6;
            this.Jmbg.TextChanged += new System.EventHandler(this.Jmbg_TextChanged);
            // 
            // PotvrdiDugme
            // 
            this.PotvrdiDugme.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PotvrdiDugme.Location = new System.Drawing.Point(24, 243);
            this.PotvrdiDugme.Name = "PotvrdiDugme";
            this.PotvrdiDugme.Size = new System.Drawing.Size(75, 23);
            this.PotvrdiDugme.TabIndex = 3;
            this.PotvrdiDugme.Text = "Potvrdi";
            this.PotvrdiDugme.UseVisualStyleBackColor = false;
            this.PotvrdiDugme.Click += new System.EventHandler(this.PotvrdiDugme_Click);
            // 
            // OdustaniDugme
            // 
            this.OdustaniDugme.Location = new System.Drawing.Point(158, 243);
            this.OdustaniDugme.Name = "OdustaniDugme";
            this.OdustaniDugme.Size = new System.Drawing.Size(75, 23);
            this.OdustaniDugme.TabIndex = 4;
            this.OdustaniDugme.Text = "Odustani";
            this.OdustaniDugme.UseVisualStyleBackColor = true;
            this.OdustaniDugme.Click += new System.EventHandler(this.OdustaniDugme_Click);
            // 
            // Adresa
            // 
            this.Adresa.Location = new System.Drawing.Point(74, 185);
            this.Adresa.Name = "Adresa";
            this.Adresa.Size = new System.Drawing.Size(100, 20);
            this.Adresa.TabIndex = 5;
            this.Adresa.TextChanged += new System.EventHandler(this.Adresa_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(101, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Jmbg";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(101, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Adresa";
            // 
            // UnosClanova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 325);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Adresa);
            this.Controls.Add(this.OdustaniDugme);
            this.Controls.Add(this.PotvrdiDugme);
            this.Controls.Add(this.Jmbg);
            this.Controls.Add(this.PrezimeClana);
            this.Controls.Add(this.ImeClana);
            this.Name = "UnosClanova";
            this.Text = "Unos clanova";
            this.Load += new System.EventHandler(this.UnosClanova_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ImeClana;
        private System.Windows.Forms.TextBox PrezimeClana;
        private System.Windows.Forms.TextBox Jmbg;
        private System.Windows.Forms.Button PotvrdiDugme;
        private System.Windows.Forms.Button OdustaniDugme;
        private System.Windows.Forms.TextBox Adresa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}
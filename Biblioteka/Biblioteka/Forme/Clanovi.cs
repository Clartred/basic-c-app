﻿using Biblioteka.Model;
using Biblioteka.Servis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class Clanovi : Form
    {
        private Clan clanZaIzmenu;
        string JmbgZaBrisanje = null;

        public Clanovi()
        {
            InitializeComponent();
        }

        private void UnesiClana_Click(object sender, EventArgs e)
        {
            var frm = new UnosClanova();
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.ShowDialog();
            PopuniTabelu();
        }

        private void ObrisiClana_Click(object sender, EventArgs e)
        {
            
            if (JmbgZaBrisanje != null)
            {
                new ClanServis().IzbrisiClana(JmbgZaBrisanje);
            }
            JmbgZaBrisanje = null;
            PopuniTabelu();
        }

        private void IzmeniClana_Click(object sender, EventArgs e)
        {
            //Objasniti sta znaci
            this.Hide();
            var frm = new IzmeniClana();
            frm.clan= clanZaIzmenu;
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.ShowDialog();
            PopuniTabelu();
        }

        private void PrikaziKnjigeClana_Click(object sender, EventArgs e)
        {

        }

        private void TabelaClanova_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Clan c =  KonvertujCelijuUClana(e.RowIndex);
            this.clanZaIzmenu = c;
            JmbgZaBrisanje = c.Jmbg;
            MessageBox.Show(c.Jmbg + " " + c.Ime + " " + c.Prezime + " " + c.Adresa);
        }
        private Clan KonvertujCelijuUClana(int i)
        {
            string jmbg = TabelaClanova.Rows[i].Cells["jmbg"].Value.ToString();
            string ime = TabelaClanova.Rows[i].Cells["ime"].Value.ToString();
            string prezime = TabelaClanova.Rows[i].Cells["prezime"].Value.ToString();
            string adresa = TabelaClanova.Rows[i].Cells["adresa"].Value.ToString();

            Clan clan = new Clan(jmbg, ime, prezime, adresa);
            return clan;
            
            /*Ovo je karaca verzija ---->
            return new Clan(
                (TabelaClanova.Rows[i].Cells["ime"].Value.ToString()),
                (TabelaClanova.Rows[i].Cells["prezime"].Value.ToString()),
                (TabelaClanova.Rows[i].Cells["jmbg"].Value.ToString()),
                (TabelaClanova.Rows[i].Cells["adresa"].Value.ToString())
     
               );*/
            
        }

        private Clan KonvertujClana(DataRowView d)
        {
            return null;
        }

        private void Clanovi_Load(object sender, EventArgs e)
        {
            //Kada kliknem na formu Clanovi, otvori se ova metoda
            PopuniTabelu();
        }

        private void PopuniTabelu()
        {
            //Kraca verzija------>
            //TabelaClanova.DataSource = new ClanServis().DajSveClanove();

            ClanServis clanS = new ClanServis();
            TabelaClanova.DataSource = clanS.DajSveClanove();

        }

        private void IzadjiDugme_Click(object sender, EventArgs e)
        {
            //Kada korisnik hoce da izadje iz forme Clanovi, potrebno je da
            //klikne da dugme izadji
            this.Close();
        }

        private void Stampanje_Click(object sender, EventArgs e)
        {
            Stampanje s = new Stampanje();
            s.Stampaj(TabelaClanova);
        }
    }
}

﻿namespace Biblioteka
{
    partial class Clanovi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button PrikaziKnjigeClana;
            this.UnesiClana = new System.Windows.Forms.Button();
            this.ObrisiClana = new System.Windows.Forms.Button();
            this.IzmeniClana = new System.Windows.Forms.Button();
            this.TabelaClanova = new System.Windows.Forms.DataGridView();
            this.IzadjiDugme = new System.Windows.Forms.Button();
            this.Stampanje = new System.Windows.Forms.Button();
            PrikaziKnjigeClana = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaClanova)).BeginInit();
            this.SuspendLayout();
            // 
            // PrikaziKnjigeClana
            // 
            PrikaziKnjigeClana.Location = new System.Drawing.Point(31, 306);
            PrikaziKnjigeClana.Name = "PrikaziKnjigeClana";
            PrikaziKnjigeClana.Size = new System.Drawing.Size(114, 46);
            PrikaziKnjigeClana.TabIndex = 4;
            PrikaziKnjigeClana.Text = "Prikazi knjige clana";
            PrikaziKnjigeClana.UseVisualStyleBackColor = true;
            PrikaziKnjigeClana.Click += new System.EventHandler(this.PrikaziKnjigeClana_Click);
            // 
            // UnesiClana
            // 
            this.UnesiClana.Location = new System.Drawing.Point(31, 29);
            this.UnesiClana.Name = "UnesiClana";
            this.UnesiClana.Size = new System.Drawing.Size(114, 51);
            this.UnesiClana.TabIndex = 0;
            this.UnesiClana.Text = "Unesi clana";
            this.UnesiClana.UseVisualStyleBackColor = true;
            this.UnesiClana.Click += new System.EventHandler(this.UnesiClana_Click);
            // 
            // ObrisiClana
            // 
            this.ObrisiClana.Location = new System.Drawing.Point(31, 96);
            this.ObrisiClana.Name = "ObrisiClana";
            this.ObrisiClana.Size = new System.Drawing.Size(114, 49);
            this.ObrisiClana.TabIndex = 1;
            this.ObrisiClana.Text = "Obrisi clana";
            this.ObrisiClana.UseVisualStyleBackColor = true;
            this.ObrisiClana.Click += new System.EventHandler(this.ObrisiClana_Click);
            // 
            // IzmeniClana
            // 
            this.IzmeniClana.Location = new System.Drawing.Point(31, 160);
            this.IzmeniClana.Name = "IzmeniClana";
            this.IzmeniClana.Size = new System.Drawing.Size(114, 50);
            this.IzmeniClana.TabIndex = 2;
            this.IzmeniClana.Text = "Izmeni Clana";
            this.IzmeniClana.UseVisualStyleBackColor = true;
            this.IzmeniClana.Click += new System.EventHandler(this.IzmeniClana_Click);
            // 
            // TabelaClanova
            // 
            this.TabelaClanova.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaClanova.Location = new System.Drawing.Point(164, -1);
            this.TabelaClanova.Name = "TabelaClanova";
            this.TabelaClanova.Size = new System.Drawing.Size(612, 407);
            this.TabelaClanova.TabIndex = 5;
            this.TabelaClanova.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelaClanova_CellContentClick);
            // 
            // IzadjiDugme
            // 
            this.IzadjiDugme.Location = new System.Drawing.Point(31, 358);
            this.IzadjiDugme.Name = "IzadjiDugme";
            this.IzadjiDugme.Size = new System.Drawing.Size(101, 48);
            this.IzadjiDugme.TabIndex = 6;
            this.IzadjiDugme.Text = "Izadji";
            this.IzadjiDugme.UseVisualStyleBackColor = true;
            this.IzadjiDugme.Click += new System.EventHandler(this.IzadjiDugme_Click);
            // 
            // Stampanje
            // 
            this.Stampanje.Location = new System.Drawing.Point(31, 227);
            this.Stampanje.Name = "Stampanje";
            this.Stampanje.Size = new System.Drawing.Size(114, 45);
            this.Stampanje.TabIndex = 8;
            this.Stampanje.Text = "Stampanje";
            this.Stampanje.UseVisualStyleBackColor = true;
            this.Stampanje.Click += new System.EventHandler(this.Stampanje_Click);
            // 
            // Clanovi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Stampanje);
            this.Controls.Add(this.IzadjiDugme);
            this.Controls.Add(this.TabelaClanova);
            this.Controls.Add(PrikaziKnjigeClana);
            this.Controls.Add(this.IzmeniClana);
            this.Controls.Add(this.ObrisiClana);
            this.Controls.Add(this.UnesiClana);
            this.Name = "Clanovi";
            this.Text = "Clanovi";
            this.Load += new System.EventHandler(this.Clanovi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaClanova)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button UnesiClana;
        private System.Windows.Forms.Button ObrisiClana;
        private System.Windows.Forms.Button IzmeniClana;
        private System.Windows.Forms.DataGridView TabelaClanova;
        private System.Windows.Forms.Button IzadjiDugme;
        private System.Windows.Forms.Button Stampanje;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteka.Model;
using Biblioteka.Servis;

namespace Biblioteka
{
    public partial class IzmeniClana : Form
    {
        public Clan clan;

        public IzmeniClana()
        {
            InitializeComponent();
        }

        private void IzmeniClana_Load(object sender, EventArgs e)
        {
            this.Ime.Text = this.clan.Ime;
            this.Prezime.Text = this.clan.Prezime;
            this.Adresa.Text = this.clan.Adresa;
            }

        private void OdustaniDugme_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PotvrdiDugme_Click(object sender, EventArgs e)
        {
            ClanServis cs = new ClanServis();
            Clan c = new Clan(this.clan.Jmbg, this.Ime.Text, this.Prezime.Text, this.Adresa.Text);
            cs.IzmeniClana(c);
            this.Close();
        }
    }
}

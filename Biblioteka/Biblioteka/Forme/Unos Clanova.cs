﻿using Biblioteka.Model;
using Biblioteka.Servis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class UnosClanova : Form
    {
        public UnosClanova()
        {
            InitializeComponent();
        }

        private void PotvrdiDugme_Click(object sender, EventArgs e)
        {
            if (Jmbg.Text != "" && ImeClana.Text != "" && PrezimeClana.Text != "" && Adresa.Text != "")
            {

                Clan clan = new Clan(Jmbg.Text, ImeClana.Text, PrezimeClana.Text, Adresa.Text);
                ClanServis clanServis = new ClanServis();
                clanServis.UnesiClana(clan);
                //MessageBox.Show(); metoda pomocu koje korisniku prikazujemo odredjenu poruku
                MessageBox.Show("Uspesno ste uneli novog clana");
                this.Close();

            }else if (Jmbg.Text == "" || ImeClana.Text == "" || PrezimeClana.Text == "" || Adresa.Text == "")
            {
                MessageBox.Show("Morate popuniti sva polja!");
        
            }
        }

        private void OdustaniDugme_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ImeClana_TextChanged(object sender, EventArgs e)
        {

        }

        private void PrezimeClana_TextChanged(object sender, EventArgs e)
        {

        }

        private void Jmbg_TextChanged(object sender, EventArgs e)
        {

        }

        private void Adresa_TextChanged(object sender, EventArgs e)
        {

        }

        private void UnosClanova_Load_1(object sender, EventArgs e)
        {

        }
    }
}

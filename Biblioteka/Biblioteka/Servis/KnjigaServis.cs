﻿using Biblioteka.Model;
using Biblioteka.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Servis
{
    class KnjigaServis
    {
        public void UnesiKnjigu(Knjiga knjiga)
        {
            KnjigaRepository repository = new KnjigaRepository();
            repository.UnesiKnjigu(knjiga);
        }

        public void IzbrisiKnjigu(int id)
        {
            KnjigaRepository repository = new KnjigaRepository();
            repository.IzbrisiKnjigu(id);
        }

        public void IzmeniKnjigu(Knjiga knjiga)
        {
            KnjigaRepository repository = new KnjigaRepository();
            repository.IzmeniKnjigu(knjiga);
        }

        public void IzdavanjeKnjige(int id)
        {
            KnjigaRepository repository = new KnjigaRepository();
            repository.IzdavanjeKnjige(id);
        }

        public void VracanjeKnjige(int id)
        {
            KnjigaRepository repository = new KnjigaRepository();
            repository.VracanjeKnjige(id);
        }

        public Knjiga DajKnjigu(int id)
        {
            KnjigaRepository repository = new KnjigaRepository();
            return repository.DajKnjigu(id);
        }

        public DataTable DajSveKnjige()
        {
            KnjigaRepository repository = new KnjigaRepository();
            return repository.DajSveKnjige();
        }
    }
}
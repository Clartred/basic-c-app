﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Servis
{
    class Stampanje
    {
        public void Stampaj(DataGridView dgvPrikaz)
        { //deo koji prosledjuje na štampu ono što se vidi u datagridview
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Text File|*.txt";
            var result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            StringBuilder builder = new StringBuilder();
            int rowcount = dgvPrikaz.Rows.Count;//dgvPrikaz popunimo prethodno sa filtriraj, pa samo posledimo na stampu
            int columncount = dgvPrikaz.Columns.Count;

            List<string> headerCols = new List<string>();
            for (int j = 0; j < columncount - 1; j++) //postavljanje naziva kolonama
            {
                headerCols.Add(dgvPrikaz.Columns[j].HeaderText);
            }
            builder.AppendLine(string.Join("\t\t", headerCols));

            for (int i = 0; i < rowcount - 1; i++) //izlistavanje svih kolona i redova uz baze u dgvPrikaz
            {
                List<string> cols = new List<string>();
                for (int j = 0; j < columncount - 1; j++)
                {
                    cols.Add(dgvPrikaz.Rows[i].Cells[j].Value.ToString());
                }
                builder.AppendLine(string.Join("\t\t", cols.ToArray()));
            }
            System.IO.File.WriteAllText(dialog.FileName, builder.ToString());
        }
    }
}

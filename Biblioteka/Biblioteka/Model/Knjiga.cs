﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Model
{
    public class Knjiga
    {
        public Knjiga() { }

        public Knjiga(int id, string naziv, string autor, string zanr, string izdavac, int brojStranica)
        {
            this.Id = id;
            this.Naziv = naziv;
            this.Autor = autor;
            this.Zanr = zanr;
            this.Izdavac = izdavac;
            this.BrojStranica = brojStranica;
        }

        public Knjiga(string naziv, string autor,string zanr, string izdavac, int brojStranica)
        {
            this.Naziv = naziv;
            this.Autor = autor;
            this.Izdavac = izdavac;
            this.Zanr = zanr;
            this.BrojStranica = brojStranica;
        }

        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Autor { get; set; }
        public string Zanr { get; set; }
        public string Izdavac { get; set; }
        public int BrojStranica { get; set; }
    }
}
